DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # script directory
cd $DIR

kill `lsof -ti tcp:8000`  # kills python server
python -m http.server 8000 &

echo `lsof -i tcp:8000`
open http://localhost:8000/face_name.html
sleep 5
kill `lsof -ti tcp:8000`  # kills python server
exit
